const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {

		let foundAlias = exchangeRates.find((currency) => {

			return currency.alias === req.body.alias;

		});

		let foundCurrency = exchangeRates.find((currency) => {

			return currency.alias === req.body.alias && currency.name === req.body.name;

		});

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				error : "Bad Request: missing required parameter <alias>"
			})
		}

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				error : "Bad Request: missing required parameter <name>"
			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				error : "Bad Request: missing required parameter <ex>"
			})
		}

		if(typeof(req.body.alias) != 'string'){
			return res.status(400).send({
				error : "Bad Request: parameter <alias> is not a string"
			})
		}

		if(typeof(req.body.name) != 'string'){
			return res.status(400).send({
				error : "Bad Request: parameter <name> is not a string"
			})
		}

		if(typeof(req.body.ex) != 'object'){
			return res.status(400).send({
				error : "Bad Request: parameter <ex> is not an object"
			})
		}

		if(req.body.alias.length === 0){
			return res.status(400).send({
				error : "Bad Request: parameter <alias> cannot be empty"
			})
		}

		if(req.body.name.length === 0){
			return res.status(400).send({
				error : "Bad Request: parameter <name> cannot be empty"
			})
		}

		if(Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				error : "Bad Request: parameter <ex> cannot be empty"
			})
		}

		if(foundAlias){
			return res.status(400).send({
				error : "Bad Request: Alias already exists."
			})
		} else if (foundCurrency) {
			return res.status(400).send({
				error : "Bad Request: Currency already exists."
			})
		} else {
			return res.status(200).send(exchangeRates);
		}

	})

module.exports = router;

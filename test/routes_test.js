const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {

	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})

	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})

/* 1 - post /currency running */
	it('1_test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "yenyen",
			name: "Japanese Yen",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})

/* 2 - name is missing */
	it('2_test_api_post_currency_returns_400_if_no_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "yen",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 3 - name is not string */
	it('3_test_api_post_currency_returns_400_if_name_is_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "yen",
			name: true,
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 4 - name is empty string */
	it('4_test_api_post_currency_returns_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "yen",
			name: "",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 5 - ex is missing */
	it('5_test_api_post_currency_returns_400_if_no_ex', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "yen",
			name: "Japanese Yen"
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 6 - ex is not an object */
	it('6_test_api_post_currency_returns_400_if_ex_is_not_object', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "yen",
			name: "Japanese Yen",
			ex: false
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 7 - ex is empty object */
	it('7_test_api_post_currency_returns_400_if_ex_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "yen",
			name: "Japanese Yen",
			ex: {
				'peso': "",
				'usd': "",
				'won': "",
				'yuan': ""
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 8 - alias is missing */
	it('8_test_api_post_currency_returns_400_if_no_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			name: "Japanese Yen",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 9 - alias is not string */
	it('9_test_api_post_currency_returns_400_if_alias_is_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: false,
			name: "Japanese Yen",
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 10 - alias is empty */
	it('10_test_api_post_currency_returns_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "",
			name: "Japanese Yen",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 11 - duplicate alias */
	it('11_test_api_post_currency_returns_400_if_duplicate_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "usd",
			name: "United States Dollar",
			ex: {
			  'peso': 50.73,
			  'won': 1187.24,
			  'yen': 108.63,
			  'yuan': 7.03
		  }
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

/* 12 - no duplicates */
	it('12_test_api_post_currency_returns_200_if_no_dupes', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "nzd",
			name: "New Zealand Dollar",
			ex: {
				'peso': 34.77,
				'usd': 0.62,
				'won': 827.93,
				'yuan': 4.25
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})


})

